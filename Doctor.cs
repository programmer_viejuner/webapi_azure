using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace webapi_doctor.Models
{
    [Table("DOCTOR")]
    public class Doctor
    {

        [Key]
        [Column("HOSPITAL_COD")]
        
        public String HOSPITAL_COD { get; set; }
        [Column("DOCTOR_NO")]
        public String DOCTOR_NO { get; set; }
        [Column("APELLIDO")]
        public String APELLIDO { get; set; }
        [Column("ESPECIALIDAD")]
        public String ESPECIALIDAD { get; set; }
        [Column("SALARIO")]
        public int SALARIO { get; set; }

    }
}
