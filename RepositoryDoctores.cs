using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi_doctor.Data;
using webapi_doctor.Models;



namespace webapi_doctor.Repositories
{
    public class RepositoryDoctores
    {

        DoctorContext context;
        
        public RepositoryDoctores(DoctorContext context)

        {
            this.context = context;

        }

        public List<Doctor> GetDoctores()
        {
            return this.context.Doctores.ToList();

        }

        public Doctor BuscarDoctor(String idoctor)
        {

            return this.context.Doctores.SingleOrDefault(z => z.DOCTOR_NO == idoctor);


        }





    }
}
