using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi_doctor.Models;
using Microsoft.EntityFrameworkCore;


namespace webapi_doctor.Data
{
    public class DoctorContext : DbContext
    { 

        public DoctorContext(DbContextOptions options) : base(options)
        {}

        public DbSet<Doctor> Doctores { get; set; }


    
    }
}
