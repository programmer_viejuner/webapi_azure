using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi_doctor.Models;
using webapi_doctor.Repositories;


namespace webapi_doctor.Controllers
{
    [Route("api/[controller]")]
    public class HomeController : Controller
    {

        RepositoryDoctores repo;

        public  HomeController(RepositoryDoctores repo)

        {
            this.repo = repo;

        }

        [HttpGet]
        public ActionResult<List<Doctor>> Get()
        {
        return this.repo.GetDoctores();

        }

        [HttpGet("{DOCTOR_NO}")]
        public ActionResult<Doctor> Get(String docid)
        {
        return this.repo.BuscarDoctor(docid);


        }

    }

    }
